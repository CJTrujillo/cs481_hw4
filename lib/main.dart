//
// CJ Trujillo 10/1/20
// CS 481 HW4
// Animation Homework
// Displays splash screen of flutter logo then image, icons and text for pets
//  when paw is pressed the pet counter increments
// In progress: original logo, second animation
// Citation: Splash Screen: Bala Kowsalya- https://tinyurl.com/y8xa7dao
//            paw image: https://tinyurl.com/ybnzy8ya
//

import 'package:flutter/material.dart';
import 'splashScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    );
  }
}