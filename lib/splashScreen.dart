// Citation: Splash Screen: Bala Kowsalya- https://tinyurl.com/y8xa7dao
//            paw image: https://tinyurl.com/ybnzy8ya
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'home.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 0, end: 300).animate(controller)
      ..addListener(() {
        setState(() {

        });
      });
    controller.forward();

    //timer to move on to the home widget
    Timer(
        Duration(seconds: 5),
            () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => PettingWidget())));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin:EdgeInsets.symmetric(vertical: 10),
        height: animation.value,
        width: animation.value,
        child: Image.asset(
          'images/paw.gif',
          width: 100,
          height: 100,
        ),
      ),
    );
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }
}
